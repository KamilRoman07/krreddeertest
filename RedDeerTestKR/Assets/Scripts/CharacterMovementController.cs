﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementController : Singleton<CharacterMovementController>
{
    [Header("--OBJECTS--")]
    [SerializeField] protected CharacterController characterController;
    [SerializeField] protected Transform cameraTransform;
    [SerializeField] protected Transform mainPlayerCamera;

    [Header("--CONTROLLS--")]
    [SerializeField] KeyCode moveForward = KeyCode.W;
    [SerializeField] KeyCode moveBackward = KeyCode.S;
    [SerializeField] KeyCode strifeLeft = KeyCode.A;
    [SerializeField] KeyCode strifeRight = KeyCode.D;
    [SerializeField] KeyCode jump = KeyCode.Space;
    [SerializeField] KeyCode interact = KeyCode.E;
    [SerializeField] KeyCode sprint = KeyCode.LeftShift;
    [SerializeField] KeyCode fire = KeyCode.Mouse0;

    [Header("--VARIABLES--")]
    [SerializeField] float walkingMovementSpeed = 2f;
    [SerializeField] float runningMovementSpeed = 2f;

    [SerializeField] private float baselineHeight = 1.75f;

    [SerializeField] private float jumpHeight = 1.0f;
    [SerializeField] private float gravityValue = -9.81f;

    float currentCameraHeight;
    float starCameraHeight;
    float yOffset = 0.1f;

    bool groundedPlayer;
    bool enableGravity = true;
    Vector3 playerVelocity = Vector3.zero;

    GameManager gameManagerInstance;
    InventoryManager inventoryManagerInstance;

    private void Awake()
    {
        characterController.height = baselineHeight;
        currentCameraHeight = starCameraHeight = cameraTransform.position.y;
        gameManagerInstance = GameManager.Instance;
        inventoryManagerInstance = InventoryManager.Instance;
    }

    void Update()
    {
        if (gameManagerInstance.GameStarted)
        {
            HandleJump();
            HandleMovementControlls();
            HandleInteractButton();
            HandleShoot();
        }
    }

    #region Handle
    void HandleJump()
    {
        groundedPlayer = characterController.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0 && enableGravity)
        {
            playerVelocity.y = gravityValue * 0.1f;
        }

        if (Input.GetKeyDown(jump) && groundedPlayer)
        {
            GameManager.Instance.IncreaseJumps();
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
    }

    void HandleMovementControlls()
    {
        float speed = walkingMovementSpeed *
                (Input.GetKey(sprint) ? runningMovementSpeed : 1);

        Vector3 movement =  new Vector3(InputConverter.ConvertToIntNegative(strifeLeft) + InputConverter.ConvertToIntPositive(strifeRight),
            0,
            InputConverter.ConvertToIntPositive(moveForward) + InputConverter.ConvertToIntNegative(moveBackward));
        movement = Vector3.ClampMagnitude(movement, 1.0f);

        if (movement.magnitude > 0.0f)
            characterController.Move(transform.rotation * movement * walkingMovementSpeed * Time.deltaTime * speed);

        characterController.Move(playerVelocity * Time.deltaTime);
    }

    void HandleInteractButton()
    {
        if (Input.GetKeyDown(interact) && RaycastManager.Instance.GetRaycastHitObject())
            if (RaycastManager.Instance.GetRaycastHitObject().TryGetComponent(out InteractableObject iObject))
                iObject.Interact();

            
    }

    void HandleShoot()
    {
        if (Input.GetKey(fire))
        {
            if (inventoryManagerInstance.IsGunEquiped)
                inventoryManagerInstance.GetCurentGun().Shoot();
        }

        if (Input.GetKeyUp(fire))
        {
            if (inventoryManagerInstance.IsGunEquiped)
                inventoryManagerInstance.GetCurentGun().StopShoot();
        }
    }
    #endregion

    internal Transform GetPlayerCameraTransform()
    {
        return mainPlayerCamera;
    }

    public void ChangeGravity(bool gravity)
    {
        enableGravity = gravity;
    }
}
