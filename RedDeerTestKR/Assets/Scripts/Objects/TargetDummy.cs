﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDummy : MonoBehaviour
{
    [SerializeField] float hp = 10;

    public void GetHit(BulletData bulletData)
    {
        hp -= bulletData.damage;
        if (hp <= 0)
            Die();
    }

    void Die()
    {
        GameManager.Instance.IncreaseShotTargets();
        Destroy(gameObject);
    }
}
