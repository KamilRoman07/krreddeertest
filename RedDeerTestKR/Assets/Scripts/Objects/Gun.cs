﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : InteractableObject
{
    [SerializeField] Transform bulletStartPoint;
    GunDataBase myData;
    bool canShoot = true;
    bool unlockOnce = true;

    public override void Awake()
    {
        base.Awake();
        myData = Data<GunDataBase>();
    }

    public override void Update()
    {
        
    }

    public void Shoot()
    {
        if (canShoot) {
            canShoot = false;
            switch (myData.fireRateType)
            {
                case GunDataBase.GunFireRateType.Auto:
                    ShootTheBullet();
                    Invoke("UnlockShoot", myData.fireRate);
                    break;
                case GunDataBase.GunFireRateType.Semi_Auto:
                    for(int i = 0; i < myData.bulletsPerBurst; i++)
                        StartCoroutine(ShootTheBulletInBurst(i * myData.delayBetweenBulletsInBurst));
                    break;
                case GunDataBase.GunFireRateType.Standard:
                    ShootTheBullet();
                    break;
            }
        }
    }

    public void StopShoot()
    {
        if (unlockOnce)
        {
            unlockOnce = false;
            Invoke("UnlockShoot", myData.fireRate);
        }
    }

    private IEnumerator ShootTheBulletInBurst(float delay)
    {
        yield return new WaitForSeconds(delay);
        ShootTheBullet();
    }

    void ShootTheBullet()
    {
        GameObject bulletObject = Instantiate(myData.bulletPrefab);
        if (bulletObject.TryGetComponent(out Rigidbody bulletRigidbody))
        {
            bulletObject.transform.position = bulletStartPoint.position;
            bulletRigidbody.AddForce(CharacterMovementController.Instance.GetPlayerCameraTransform().forward * myData.forceMultiplier, ForceMode.Impulse);
        }
        else
        {
            Debug.LogError("There is an error with bullet - no rigidbody");
            Destroy(bulletObject);
        }
    }

    void UnlockShoot()
    {
        canShoot = true;
        unlockOnce = true;
    }
}
