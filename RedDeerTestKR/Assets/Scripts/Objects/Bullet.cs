﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : InteractableObject
{
    [SerializeField] float lifeSpan = 5f;
    BulletData myData;

    public override void Awake()
    {
        base.Awake();
        myData = Data<BulletData>();
        Invoke("Die", lifeSpan);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.TryGetComponent(out TargetDummy dummy))
        {
            dummy.GetHit(myData);
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
