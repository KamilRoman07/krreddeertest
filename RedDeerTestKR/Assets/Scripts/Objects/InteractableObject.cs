﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    InteractableObjectData Data();
    InteractableObjectData OriginalData();
    void Interact();
    void StopInteract();
}

public class InteractableObject : MonoBehaviour, IInteractable
{
    [SerializeField] private InteractableObjectData data;
    private InteractableObjectData originalData;

    public virtual void Awake()
    {
        SetData();
    }

    protected void SetData()
    {
        if (data != null && !data.name.Contains("Clone"))
        {
            originalData = data;
            data = Instantiate(originalData);
        }
    }

    public virtual void SetData(InteractableObjectData dataToSet)
    {
        data = dataToSet;
    }

    public virtual void Update()
    {
    }

    public virtual void Interact()
    {
    }

    public virtual void StopInteract()
    {
    }

    public virtual InteractableObjectData Data()
    {
        return data;
    }

    public virtual T Data<T>() where T : InteractableObjectData
    {
        return data as T;
    }

    public virtual InteractableObjectData OriginalData()
    {
        return originalData;
    }

    public virtual void SetMouseOver()
    {
        Debug.LogError("Raycasting " + gameObject.name);
    }
}
