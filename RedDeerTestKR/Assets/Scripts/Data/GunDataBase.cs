﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GunDataBase", menuName = "Data/GunDataBase", order = 1)]
public class GunDataBase : InteractableObjectData
{
    public enum GunFireRateType
    {
        Standard,
        Semi_Auto,
        Auto
    }

    [Header("--GUN BASE SPECIFIC--")]
    public GunFireRateType fireRateType = GunFireRateType.Standard;
    public float forceMultiplier = 500f;
    public int bulletsPerBurst = 3;
    public float delayBetweenBulletsInBurst = 0.25f;
    public float fireRate = 1f; //delay between bullets

    public GameObject bulletPrefab;
   
}
