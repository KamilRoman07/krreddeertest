﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletData", menuName = "Data/BulletData", order = 1)]
public class BulletData : InteractableObjectData
{
    public float damage = 10f;
}
