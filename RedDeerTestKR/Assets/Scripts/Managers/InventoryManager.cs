﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : Singleton<InventoryManager>
{
    [SerializeField] List<Gun> playerSetupGunList;
    List<Gun> playerGunList;
    int curentGunIndex = -1;

    private void Awake()
    {
        playerGunList = new List<Gun>(playerSetupGunList);
        if (playerGunList.Count > 0)
            curentGunIndex = 0;
    }

    public bool IsGunEquiped
    {
        get
        {
            return curentGunIndex >= 0;
        }
    }

    public Gun GetCurentGun()
    {
        return playerGunList[curentGunIndex];
    }
}
