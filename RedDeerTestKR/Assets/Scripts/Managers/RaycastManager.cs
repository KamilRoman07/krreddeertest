﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class RaycastManager : Singleton<RaycastManager>
{
    RaycastHit hit;
    Transform mainCameraTransform;
    private Collider previous;
    bool castRay = true;
    int ignoreLayers = 0;

    public bool IsCasting => castRay;

    public RaycastHit GetRaycastHit() => hit;
    public Vector3 GetRaycastHitPoint() => hit.point;
    public Collider GetRaycastHitObject() => hit.collider;
    public InteractableObject lastSeenObject { get; private set; }
    public event Action<Collider> OnRayEnter;
    public event Action<Collider> OnRayStay;
    public event Action<Collider> OnRayExit;

    [SerializeField] private float raycastLenght = 2f;

    void Start()
    {
        mainCameraTransform = CharacterMovementController.Instance.GetPlayerCameraTransform();
        ignoreLayers = LayerMask.GetMask(Globals.raycastIgnorableLayers);
        this.OnRayEnter += Ray_OnEnter;
        this.OnRayExit += Ray_OnExit;
    }

    // Update is called once per frame
    void Update()
    {
        if (castRay && mainCameraTransform != null)
           CastRay(mainCameraTransform.position, mainCameraTransform.forward);
    }

    private Collider CastRay(Vector3 originPoint, Vector3 direction)
    {
        Utility.ClosestPriorityHit(new Ray(originPoint, direction), raycastLenght, out hit, ~ignoreLayers, new string[] { "Player" }, false, new string[] { });

        if (hit.collider)
        {
            RaycastHit testHit = default;
            Utility.ClosestPriorityHit(new Ray(originPoint, direction), raycastLenght, out testHit, ~ignoreLayers, new string[] { "Player" }, false, new string[] { });

            if (hit.collider != null && hit.collider.gameObject.TryGetComponent(out InteractableObject io))
                hit.collider.gameObject.GetComponent<InteractableObject>().SetMouseOver();

            if (testHit.collider)
                hit = testHit;
        }

        ProcessCollision(hit.collider);

        if (hit.collider && hit.collider.TryGetComponent<InteractableObject>(out InteractableObject iObj) && iObj != lastSeenObject)
            lastSeenObject = iObj;

        return hit.collider;
    }

    private void ProcessCollision(Collider current)
    {
        // No collision this frame.
        if (current == null)
        {
            // But there was an object hit last frame.
            if (previous != null)
                DoEvent(OnRayExit, previous);
        }

        // The object is the same as last frame.
        else if (previous == current)
        {
            DoEvent(OnRayStay, current);
        }

        // The object is different than last frame.
        else if (previous != null)
        {
            DoEvent(OnRayExit, previous);
            DoEvent(OnRayEnter, current);
        }

        // There was no object hit last frame.
        else
        {
            DoEvent(OnRayEnter, current);
        }

        // Remember this object for comparing with next frame.
        previous = current;
    }

    private void DoEvent(Action<Collider> action, Collider collider)
    {
        action?.Invoke(collider);
    }

    private void Ray_OnEnter(Collider collider)
    {
        collider.TryGetComponent<InteractableObject>(out InteractableObject iObject);
        if (iObject)
        {
           Debug.LogError("Object in view: " + iObject.name);
        }
    }

    private void Ray_OnExit(Collider collider)
    {
        collider.TryGetComponent<InteractableObject>(out InteractableObject iObject);
        if (iObject)
        {

        }
    }

    public void StopCasting()
    {
        castRay = false;
    }

    public void CastRay()
    {
        castRay = true;
    }
}
