﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollector : Singleton<ObjectCollector>
{
    [HideInInspector]
    public List<Bullet> bullets = new List<Bullet>();
}
