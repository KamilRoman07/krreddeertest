﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] GameObject liftShaft, taskQuestionPanel;

    int playerJumps;
    int playerShotTargets;
    System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
    StatisticController statisticsControllerInstance;
    bool gameStarted = false;
    public bool GameStarted => gameStarted;

    private void Awake()
    {
        playerJumps = playerShotTargets = 0;
        timer.Start();
        statisticsControllerInstance = StatisticController.Instance;
        statisticsControllerInstance.UpdateJumps(playerJumps);
        statisticsControllerInstance.UpdateShotObjects(playerShotTargets);
    }

    private void Update()
    {
        statisticsControllerInstance.UpdateTime(timer.Elapsed.ToString("c"));
    }

    public void IncreaseJumps()
    {
        playerJumps++;
        statisticsControllerInstance.UpdateJumps(playerJumps);
    }

    public void IncreaseShotTargets()
    {
        playerShotTargets++;
        statisticsControllerInstance.UpdateShotObjects(playerShotTargets);
    }

    public void StartGame(bool variant)
    {
        liftShaft.SetActive(variant);
        taskQuestionPanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        gameStarted = true;
    }
}
