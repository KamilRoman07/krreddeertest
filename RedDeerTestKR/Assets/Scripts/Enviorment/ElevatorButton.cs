﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorButton : InteractableObject
{
    ElevatorController elevatorController;
    ElevatorFloor elevatorFloor;

    internal void SetUp(ElevatorFloor floor, ElevatorController elevatorController)
    {
        this.elevatorController = elevatorController;
        elevatorFloor = floor;
    }

    public override void Interact()
    {
        base.Interact();
        elevatorController.CallElevator(elevatorFloor);
    }
}
