﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] List<TargetDummy> targetDummies;
    bool activated = false;

    private void OnTriggerEnter(Collider collider)
    {
        if(targetDummies.Count > 0 && collider.gameObject.CompareTag("Player") && !activated)
        {
            activated = true;
            foreach (TargetDummy dummy in targetDummies)
                dummy.gameObject.SetActive(true);
        }
    }
}
