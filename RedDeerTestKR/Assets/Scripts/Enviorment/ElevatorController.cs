﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour
{
    [Header("--SETUP VARIABLES--")]
    [SerializeField] List<ElevatorFloor> elevatorFloors;
    [Header("--SETUP OBJECTS--")]
    [SerializeField] GameObject elevator;
    [SerializeField] Transform elevatorPanel;
    [SerializeField] GameObject elevatorButtonPrefab;

    List<ElevatorButton> elevatorButtons = new List<ElevatorButton>();

    bool elevatorInMotion = false;

    private void Awake()
    {
        CreateElevatorButtons();
    }

    void CreateElevatorButtons()
    {
        var i = 0;
        var offset = new Vector3(-0.7f, 0.1f, 0f);
        foreach(ElevatorFloor floor in elevatorFloors)
        {
            GameObject buttonObject = Instantiate(elevatorButtonPrefab);
            buttonObject.transform.SetParent(elevatorPanel, true);
            buttonObject.transform.localPosition = new Vector3(-0.7f, 0.15f * i, 0f);
            if (buttonObject.TryGetComponent(out ElevatorButton elevatorButton))
                elevatorButton.SetUp(floor, this);
            i++;
        }
    }

    public void CallElevator(ElevatorFloor floor)
    {
        if (!elevatorInMotion)
        {
            elevatorInMotion = true;
            LeanTween.move(elevator, floor.transform.position, 2f).setOnComplete(() => {
                elevatorInMotion = false;
            });
        }
    }
}
