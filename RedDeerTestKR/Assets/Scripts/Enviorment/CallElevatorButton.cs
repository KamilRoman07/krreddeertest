﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallElevatorButton : InteractableObject
{
    [SerializeField] ElevatorFloor elevatorFloor;
    ElevatorController elevatorController;

    public override void Awake()
    {
        base.Awake();
        elevatorController = FindElevatorControllerInParent(gameObject);
    }

    public override void Interact()
    {
        base.Interact();
        CallElevator();
    }

    void CallElevator()
    {
        elevatorController.CallElevator(elevatorFloor);
    }

    public ElevatorController FindElevatorControllerInParent(GameObject checkObject)
    {
        if (checkObject.transform.parent.TryGetComponent(out ElevatorController elevatorController))
            return elevatorController;
        else if (checkObject.transform.parent != null)
            return FindElevatorControllerInParent(checkObject.transform.parent.gameObject);

        return null;
    }
}
