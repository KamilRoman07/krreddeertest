﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputConverter
{
    public static int ConvertToIntPositive(KeyCode key)
    {
        return Input.GetKey(key) ? 1 : 0;
    }

    public static int ConvertToIntNegative(KeyCode key)
    {
        return Input.GetKey(key) ? -1 : 0;
    }
    public static float MoveCameraLeftRight()
    {
        return Input.GetAxis("Mouse X");
    }

    public static float MoveCameraUpDown()
    {
        return Input.GetAxis("Mouse Y");
    }
}
