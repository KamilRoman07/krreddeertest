﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public static string[] raycastIgnorableLayers = { "Ignore Raycast", "TransparentFX", "Bullet" };
}
