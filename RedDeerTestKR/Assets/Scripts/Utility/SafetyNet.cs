﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafetyNet : MonoBehaviour
{
    [SerializeField] Transform startingPoint;

    private void OnTriggerEnter(Collider collider)
    {
        if (!startingPoint)
        {
            Debug.LogError("Staring point erased, Error!!");
            return;
        }

            
        if (collider.CompareTag("Player"))
        {
            collider.gameObject.transform.position = startingPoint.position;
        }
    }
}
