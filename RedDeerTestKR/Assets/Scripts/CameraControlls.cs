﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlls : MonoBehaviour
{
    [Header("--OBJECTS--")]
    [SerializeField] Transform cameraTransform;

    [Header("--VARIABLES--")]
    [SerializeField] float cameraSensitivityFactor = 1.0f;

    private readonly float lookDownThreshold = 83f;
    private readonly float lookUpThreshold = 300f;

    GameManager gameManagerInstance;

    private void Awake()
    {
        gameManagerInstance = GameManager.Instance;
    }

    void Update()
    {
        if(gameManagerInstance.GameStarted)
            HandleCameraRotation();
    }

    void HandleCameraRotation()
    {
        if (InputConverter.MoveCameraLeftRight() != 0)
            transform.Rotate(0, cameraSensitivityFactor * InputConverter.MoveCameraLeftRight(), 0);


        if (InputConverter.MoveCameraUpDown() != 0)
        {
            float cameraRotationX = cameraTransform.localEulerAngles.x;
            cameraRotationX += -cameraSensitivityFactor * InputConverter.MoveCameraUpDown();

            //-180 to differ minus/plus below/above horizon
            float targetRotation = cameraRotationX - 180f;

            bool isMouseMovingDown = InputConverter.MoveCameraUpDown() < 0;
            bool isMouseMovingUp = InputConverter.MoveCameraUpDown() > 0;
            bool isTargetRotationAboveHorizon = targetRotation > 0;
            bool isTargetRotationBelowHorizon = targetRotation < 0;
            bool isTargetRotationWithinDownThreshold = targetRotation < (lookDownThreshold - 180f);
            bool isTargetRotationWithinUpThreshold = targetRotation > (lookUpThreshold - 180f);

            if (isMouseMovingDown)
            {
                bool isTargetRotationAcceptable = isTargetRotationBelowHorizon && isTargetRotationWithinDownThreshold;

                if (isTargetRotationAboveHorizon || isTargetRotationAcceptable)
                    cameraTransform.Rotate(-cameraSensitivityFactor * InputConverter.MoveCameraUpDown(), 0, 0);
                else if (!isTargetRotationAcceptable)
                    cameraTransform.localEulerAngles = new Vector3(lookDownThreshold, cameraTransform.localEulerAngles.y, cameraTransform.localEulerAngles.z);
            }
            if (isMouseMovingUp)
            {
                bool isTargetRotationAcceptable = isTargetRotationAboveHorizon && isTargetRotationWithinUpThreshold;

                if (isTargetRotationBelowHorizon || isTargetRotationAcceptable)
                    cameraTransform.Rotate(-cameraSensitivityFactor * InputConverter.MoveCameraUpDown(), 0, 0);
                else if (!isTargetRotationAcceptable)
                    cameraTransform.localEulerAngles = new Vector3(lookUpThreshold, cameraTransform.localEulerAngles.y, cameraTransform.localEulerAngles.z);
            }
        }
    }
}
