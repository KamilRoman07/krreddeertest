﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingingAxeTrap : MonoBehaviour
{
    [SerializeField] float swingAxeDegree;
    [SerializeField] float swingAxeDelay;
    [SerializeField] float swingAxeSpeed;
    [SerializeField] float swingAxeStartDelay = 0f;

    float currentSwingDegree;

    private void Awake()
    {
        StartCoroutine(SwingAxe(swingAxeStartDelay, swingAxeDegree));
        currentSwingDegree = swingAxeDegree;
    }

    IEnumerator SwingAxe(float delayTime, float degree)
    {
        yield return new WaitForSeconds(delayTime + swingAxeSpeed);
        currentSwingDegree = -degree;
        LeanTween.rotate(gameObject, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + currentSwingDegree), swingAxeSpeed);
        StartCoroutine(SwingAxe(swingAxeDelay, currentSwingDegree));

    }
}
