﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour
{
    [SerializeField] GameObject bumper;
    [SerializeField] Transform reloadedSpot, bumpSpot;
    [SerializeField] float bumpTime, retractTime;
    [SerializeField] float bumpRecharge, retractDelay;
    [SerializeField] float bumperStartDelay = 0f;
    [Header("--MAKE RANDOM? ^^--")]
    [SerializeField] bool random = false;
    [SerializeField] float bumpRandomRechargeMin, bumpRandomRechargeMax;

    private void Awake()
    {
        StartCoroutine(Bump(bumperStartDelay));
    }

    public IEnumerator Bump(float bumpRetractDelay)
    {
        LeanTween.move(bumper, bumpSpot, bumpTime);
        yield return new WaitForSeconds(bumpTime + bumpRetractDelay);
        StartCoroutine(Retract(random ? Random.Range(bumpRandomRechargeMin, bumpRandomRechargeMax) : bumpRecharge));
    }

    public IEnumerator Retract(float bumpDelay)
    {
        LeanTween.move(bumper, reloadedSpot, retractTime);
        yield return new WaitForSeconds(bumpDelay + retractTime);
        StartCoroutine(Bump(retractDelay));
    }
}
