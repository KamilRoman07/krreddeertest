﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonTrap : MonoBehaviour
{
    [SerializeField] float shotDelay = 3f;
    [SerializeField] float power = 1000f;
    [SerializeField] Vector3 direction;
    [SerializeField] GameObject cannonBallPrefab;
    GameObject cannonBall;

    private void Awake()
    {
        StartCoroutine(Shoot(shotDelay));
    }

    IEnumerator Shoot(float delay)
    {
        yield return new WaitForSeconds(delay);
        cannonBall = Instantiate(cannonBallPrefab);
        cannonBall.transform.position = transform.position;
        if (cannonBall.TryGetComponent(out Rigidbody ballRigidbody))
            ballRigidbody.AddForce(direction * power, ForceMode.Impulse);
        Invoke("DestroyBall", shotDelay);
        StartCoroutine(Shoot(shotDelay+0.1f));
    }

    void DestroyBall()
    {
        Destroy(cannonBall);
    }
}
