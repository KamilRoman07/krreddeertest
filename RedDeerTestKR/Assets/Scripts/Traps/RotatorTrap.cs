﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorTrap : MonoBehaviour
{
    [SerializeField] float rotateSpeed;
    [SerializeField] float rotateDegree;
    [SerializeField] float rotateDelay;
    [SerializeField] float rotateStartDelay = 0f;

    private void Awake()
    {
        StartCoroutine(RotateTrap(rotateStartDelay, rotateDegree));
    }

    IEnumerator RotateTrap(float rotateDelay, float rotateDegree)
    {
        yield return new WaitForSeconds(rotateDelay + rotateSpeed);
        LeanTween.rotateLocal(gameObject, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + rotateDegree, transform.eulerAngles.z), rotateSpeed);
        StartCoroutine(RotateTrap(rotateDelay, rotateDegree));
    }
}
