﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatisticController : Singleton<StatisticController>
{
    [SerializeField] TextMeshProUGUI jumps, shotObjects, time;

    public void UpdateJumps(int jumpsCount)
    {
        jumps.SetText("Player jumps: " + jumpsCount);
    }

    public void UpdateShotObjects(int objectsCount)
    {
        shotObjects.SetText("Player shot objects: " + objectsCount);
    }

    public void UpdateTime(string elapsedTime)
    {
        time.SetText("Elapsed time: " + elapsedTime);
    }
}
